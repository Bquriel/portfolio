"use strict";

const toggleMenu = () => {
  if( !showMenu ){
    menuBtn.classList.add( "close" );
    menu.classList.add( "show" );
    nav.classList.add( "show" );
    branding.classList.add( "show" );
    for( let i = 0; i < navItems.length; ++i ){
      navItems[ i ].classList.add( "show" );
    }
    showMenu = true;
  } else {
    menuBtn.classList.remove( "close" );
    menu.classList.remove( "show" );
    nav.classList.remove( "show" );
    branding.classList.remove( "show" );
    for( let i = 0; i < navItems.length; ++i ){
      navItems[ i ].classList.remove( "show" );
    }
    showMenu = false;
  }
}

const menuBtn   = document.querySelector( ".menu-btn" );
const menu      = document.querySelector( ".menu" );
const nav       = document.querySelector( ".menu-nav" );
const branding  = document.querySelector( ".menu-branding" );

const navItems  = document.querySelectorAll( ".nav-item" );

// Set initial state of menu.
let showMenu = false;

menuBtn.addEventListener( "click", toggleMenu )